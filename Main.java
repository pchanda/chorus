import java.util.*;
import java.io.*;

public class Main
{
    public static void main(String args[])throws Exception
    {
        //Change your parameters here.
        String fname = "Data.txt";                       //Genotype data file name.
        String outfile = "KWII.txt";                     //Name of output files.
        int theta = 10;                                  //Buffer size
        int niter = 2;                                   //No of iterations to execute.
        int use_k = 0;                                   //Keep this 0.
        
        //Do not change anything below.
        Chorus Am = null;
        if(use_k==0)                    
            Am = new Chorus(false);           //use kernel = false;
        else
            Am = new Chorus(true);            //use kernel = true;
        double[][] D = Am.readData_continuous(fname);                
        Am.init(false,false,D.length);//must call this.
        Am.INVALID_VAL = -99;
        Am.SHOW_NEG_INTERACTIONS = false;
        System.out.println("Running AMBIENCE search with theta = "+theta+" and #iterations = "+niter);
        Am.search(D,theta,niter,outfile);        
    }    
}
