import java.util.*;

public class Combination
{
	String comb_str; //the combination as a string e.g. 1|2|3|.
	Vector comb_vec; //the combination as a vector.
	double kwii;//KWII
        double pai;//PAI
        double pvalue = -100;              
        
	public Combination(String str)
	{
            comb_str = str;
            comb_vec = convert(str);
            kwii = -100;
            pai = -100;
	}

	public Combination(Vector vec)
	{
            comb_str = convert(vec);
            comb_vec = vec;
            kwii = -100;
            pai = -100;
	}

	public Combination(Combination C)
	{
            comb_str = new String(C.comb_str);
            comb_vec = new Vector(C.comb_vec);
            kwii = -100;
            pai = -100;
            pvalue = C.pvalue;
	}

	public Combination()
	{
            comb_str = null;
            comb_vec = null;
            kwii = -100;
            pai = -100;
	}
        
	public void append(Integer u) throws Exception
	{
            String t = u+"";
            if(comb_vec.contains(t)==false)
            {
                int pos = -1;
                for(int i=0;i<comb_vec.size();i++)
                {
                    int a = Integer.valueOf((String)comb_vec.get(i));
                    if(u.intValue()<a)
                    {
                        pos = i;
                        break;
                    }
                }
                if(pos>=0)
                    comb_vec.add(pos,t);
                else //empty vector.
                    comb_vec.add(t);
                comb_str = convert(comb_vec);
            }
	}        

	public static Vector convert(String comb)
	{
            StringTokenizer st = new StringTokenizer(comb,"|");
            Vector V = new Vector();
            while(st.hasMoreTokens())
            {
                V.add(st.nextToken());			
            }
            return V;
	}

	public static String convert(Vector comb)
	{
            String s = "";
            for(int i=0;i<comb.size();i++)
            {
                String x = (String)comb.get(i);
                s += x + "|";
            }
            return s;
	}        

	public void print() throws Exception
	{
            System.out.print("Combination = "+comb_str+" [");
            Vector V = comb_vec;
            for(int i=0;i<V.size();i++)
            {
                System.out.print(V.get(i) +" ");
            }
            System.out.println("] kwii = "+kwii+" , pai = "+pai);
	}        
}
