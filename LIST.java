import java.util.*;
import java.io.*;

class Entry
{
	Combination comb;
	double value;
	public Entry(Combination c,double d)
	{
		comb = c;
		value = d;
	}
};

class Cmp implements Comparator
{
	public int compare(Object o1,Object o2)
	{
        Entry e1 = (Entry)o1;
		Entry e2 = (Entry)o2;
		if(e1.value<e2.value)return -1;
        else if(e1.value>e2.value)return 1;
        else return 0;
	}
};

public class LIST
{
	PriorityQueue PQ;
	HashSet H; //to store the combinations stored in PQ (helps to check duplication efficiently).
	int max_size;

	public LIST(int m)
	{
		if(m>0)
		{
			PQ = new PriorityQueue(m,new Cmp());
			H = new HashSet(m);
		}
		else
		{
			PQ = new PriorityQueue(10,new Cmp());
			H = new HashSet(10);
		}
		max_size = m;
	}

	public void clear() throws Exception
	{
		PQ.clear();
		H.clear();
	}

	public int size() throws Exception
	{
		return H.size();
	}

	public void add(Combination c) throws Exception
	{
		double v = c.pai;
		if(H.contains(c.comb_str)==false)
		{
			if(H.size()<max_size || max_size<0)//always add when max_size=-1, i.e. infinite buffer size.
			{
				Entry ent = new Entry(c,v);
				PQ.offer(ent);
				H.add(c.comb_str);
			}
			else
			{
				Entry ent = (Entry)PQ.peek();
				if(ent.value<v)
				{
					//remove the minimum one from priority queue.
					ent = (Entry)PQ.poll(); //retrieves and removes the head of this queue (the minimum one)
					H.remove(ent.comb.comb_str);
					//and then add this one.
					PQ.offer(new Entry(c,v));
					H.add(c.comb_str);
				}
				else
				{
					//nothing to do.
				}
			}
		}
	}

	public void print() throws Exception
	{
		System.out.println("LIST ->");
		Iterator I = PQ.iterator();
		while(I.hasNext())
		{
			Entry ent = (Entry)I.next();
			System.out.println(ent.comb.comb_str+" -- "+ent.comb.pai);
		}
		System.out.println();
	}

	public HashMap copy() throws Exception
	{
		//copy this PQ to a HashMap.
		HashMap X = new HashMap();
		Entry ent = null;
		while((ent=(Entry)PQ.poll())!=null)
		{
			X.put(ent.comb.comb_str,ent.comb);
			//ent.comb.print();
		}
		return X;
	}
}
